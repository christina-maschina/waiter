package hr.tvz.android.waiterapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.security.SecureRandom;
import java.util.Random;

import hr.tvz.android.waiterapp.R;

public class WaiterActivity extends AppCompatActivity {
    Button codeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiter);
        codeButton=findViewById(R.id.code_button);
        codeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int result=generateNumber();
                Intent intent=new Intent(WaiterActivity.this, RandomNumberActivity.class);
                intent.putExtra("random_number", result);
                intent.putExtra("responseID", getIntent().getLongExtra("responseID", 1));
                intent.putExtra("auth", getIntent().getStringExtra("auth"));
                startActivity(intent);
            }
        });
    }

    private int generateNumber() {
        int low=100000;
        int height=999999;
        Random rand=new Random();
        return rand.nextInt(height-low) + low;
    }
}