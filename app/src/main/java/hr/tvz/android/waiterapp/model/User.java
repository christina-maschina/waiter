package hr.tvz.android.waiterapp.model;

import com.google.gson.annotations.SerializedName;


public class User {

    @SerializedName("status")
    String status;
    @SerializedName("id")
    long  id;

    @Override
    public String toString() {
        return "User{" +
                "status='" + status + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
