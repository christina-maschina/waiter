package hr.tvz.android.waiterapp.model;

import com.google.gson.annotations.SerializedName;


public class Restaurant {

    @SerializedName("id")
    long id;
    @SerializedName("code")
    int code;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
