package hr.tvz.android.waiterapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import hr.tvz.android.waiterapp.R;
import hr.tvz.android.waiterapp.Service;
import hr.tvz.android.waiterapp.ServiceGenerator;
import hr.tvz.android.waiterapp.model.Restaurant;
import hr.tvz.android.waiterapp.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RandomNumberActivity extends AppCompatActivity {
    TextView textView;
    private static final String API_URL = "http://10.0.2.2:8080/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_number);
        textView=findViewById(R.id.random_number);
        Intent intent=getIntent();
        int num=intent.getIntExtra("random_number", 1);
        textView.setText(String.valueOf(num));
        long id=intent.getLongExtra("responseID", 1);
        String auth=intent.getStringExtra("auth");

        Restaurant restaurant=new Restaurant();
        restaurant.setId(id);
        restaurant.setCode(num);

        Service service = ServiceGenerator.createService(Service.class, API_URL, auth);
        Call<List<Restaurant>> call = service.setCode (restaurant);
        call.enqueue(new Callback<List<Restaurant>>() {
            @Override
            public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response) {
            }

            @Override
            public void onFailure(Call<List<Restaurant>> call, Throwable t) {

            }
        });
    }
}