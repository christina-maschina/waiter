package hr.tvz.android.waiterapp;


import java.util.List;
import java.util.Map;

import hr.tvz.android.waiterapp.model.Restaurant;
import hr.tvz.android.waiterapp.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Service {

    @GET("user")
    Call<User> authenticate();

    @POST("restaurant")
    Call<List<Restaurant>> setCode(@Body Restaurant restaurant);
}
