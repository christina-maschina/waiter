package hr.tvz.android.waiterapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import hr.tvz.android.waiterapp.R;
import hr.tvz.android.waiterapp.Service;
import hr.tvz.android.waiterapp.ServiceGenerator;
import hr.tvz.android.waiterapp.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final String API_URL = "http://10.0.2.2:8080/";
    EditText code;
    EditText username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.code = findViewById(R.id.code);
        this.username=findViewById(R.id.username);

        Button loginButton = (Button) findViewById(R.id.loginbutton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass=code.getText().toString();
                String user=username.getText().toString();
                String base=user + ":" + pass;

                String auth="Basic " + Base64.encodeToString(base.getBytes(), Base64.NO_WRAP);
                Log.d("AUTH", auth);

                Service service = ServiceGenerator.createService(Service.class, API_URL, auth);
                Call<User> call = service.authenticate();
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if(response.code()==401){
                            Toast.makeText(getApplicationContext(), "Unauthorized request", Toast.LENGTH_LONG).show();
                        } else{
                            Log.d("RESPONSE", String.valueOf(response.body().getId()));
                            Intent intent = new Intent(MainActivity.this, WaiterActivity.class);
                            intent.putExtra("responseID", response.body().getId());
                            intent.putExtra("auth", auth);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                    }
                });
            }
        });

    }
}